var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;

var UsuarioSchema = new Schema({
    nombre: String
});

UsuarioSchema.statics.createInstance = function(nombre) {
    return new this({
        nombre: nombre
    });
};

UsuarioSchema.statics.getAllUsuarios = function(callback) {
    return this.find({}, callback);
};

UsuarioSchema.statics.add = function(usuario, callback) {
    return this.create(usuario, callback);
};

UsuarioSchema.statics.update = function(id, usuario, callback) {
    return this.updateOne({ _id: id }, usuario, callback);
};

UsuarioSchema.statics.findById = function(id, callback) {
    return this.findOne({ _id: id }, callback);
};

UsuarioSchema.statics.deleteById = function(id, callback) {
    return this.findByIdAndDelete(id, callback);
};

UsuarioSchema.methods.reserve = function(biciId, desde, hasta, callback) {
    var reserva = new Reserva({
        desde: desde,
        hasta: hasta,
        usuario: this._id,
        bicicleta: biciId
    });

    return reserva.save(callback);
};

module.exports = mongoose.model('Usuario', UsuarioSchema);
