var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var BicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: { type: '2dsphere', sparse: true}
    }
});

BicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
}

BicicletaSchema.methods.toString = function () {
    return 'id: ' + this.id + ' | color: ' + this.color + ', modelo: ' + this.modelo;
};

BicicletaSchema.statics.getAllbicicletas = function(callback) {
    return this.find({}, callback);
};

BicicletaSchema.statics.add = function(bici, callback) {
    return this.create(bici, callback);
};

BicicletaSchema.statics.update = function(bici, callback) {
    return this.updateOne({ code: bici.code }, bici, callback);
};

BicicletaSchema.statics.findById = function(code, callback) {
    return this.findOne({ code: code }, callback);
};

BicicletaSchema.statics.deleteById = function(code, callback) {
    return this.deleteOne({ code: code }, callback);
};

module.exports = mongoose.model('Bicicleta', BicicletaSchema);

/*
var Bicicleta = mongoose.model('Bicicleta', BicicletaSchema);
module.exports = Bicicleta;

var bici1 = Bicicleta.createInstance(1, 'rojo', 'raptor', [-31.4158007,-64.1825873]);
var bici2 = Bicicleta.createInstance(2, 'azul', 'ragnarok', [-31.4153326,-64.1826589]);
var bici3 = Bicicleta.createInstance(3, 'verde', 'predator', [-31.4164758,-64.1834184]);

Bicicleta.add([bici1, bici2, bici3]);
*/