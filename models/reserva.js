var mongoose = require('mongoose');
const { DateTime } = require("luxon");
var Schema = mongoose.Schema;

var ReservaSchema = new Schema({
    desde: Date,
    hasta: Date,
    bicicleta: { type: mongoose.Schema.Types.ObjectId, ref: 'Bicicleta' },
    usuario: { type: mongoose.Schema.Types.ObjectId, ref: 'Usuario' }
});

ReservaSchema.statics.getAllReservas = function(callback) {
    return this.find({}, callback);
};

ReservaSchema.statics.findById = function(id, callback) {
    return this.findOne({ _id: id }, callback);
};

ReservaSchema.statics.deleteById = function(id, callback) {
    return this.findByIdAndDelete(id, callback);
};

ReservaSchema.methods.getReservationDays = function() {
    var hasta = DateTime.fromJSDate(this.hasta, { zone: 'utc' });
    var desde = DateTime.fromJSDate(this.desde, { zone: 'utc' });

    return hasta.diff(desde).as('days') + 1;
}

module.exports = mongoose.model('Reserva', ReservaSchema);
