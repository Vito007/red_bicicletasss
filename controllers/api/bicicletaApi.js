var Bicicleta = require('../../models/bicicleta');

module.exports.list = function(req, res) {
    Bicicleta.getAllbicicletas().then(function(bicicletas) {
        res.status(200).json({bicicletas});
    });
};

module.exports.create = function(req, res) {
    Bicicleta.getAllbicicletas().then(function(bicicletas) {
        var id = bicicletas.length + 1;
        var bici = Bicicleta.createInstance(id, req.body.color, req.body.modelo, [req.body.lat, req.body.lon]);

        // Add new Bici
        Bicicleta.add(bici).then(function(newBici) {
            res.status(201).json(newBici);
        }).catch(function(error) {
            // throw the error to let the most outside catch to handle it
            throw Error(error);
        });
    }).catch(function(error) {
        // log the error
        console.log(error);

        res.status(500).json({
            error: 'Error adding bici'
        });
    });
};

module.exports.update = function(req, res) {
    var data = {
        code: req.params.id,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lon]
    }
    
    // Update bici
    Bicicleta.update(data).then(function(response) {
        Bicicleta.findById(data.code).then(function(bici) {
            res.status(200).json(bici);
        }).catch(function(error) {
            // throw the error to let the most outside catch to handle it
            throw Error(error);
        });
    }).catch(function(error) {
        // log the error
        console.log(error);

        res.status(500).json({
            error: 'Error updating bici'
        });
    });
};

module.exports.delete = function(req, res) {
    // Delete Bici
    Bicicleta.deleteById(req.params.id).then(function() {
        res.status(204).json();
    }).catch(function(error) {
        // log the error
        console.log(error);

        res.status(500).json({
            error: 'Error deleting bici'
        });
    });
};
