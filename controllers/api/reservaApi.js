const Reserva = require('../../models/reserva');

module.exports.list = function(req, res) {
    Reserva.getAllReservas().then(function(reservas) {
        res.status(200).json({reservas});
    });
};

module.exports.days = function(req, res) {
    // Delete reserva
    Reserva.findById(req.params.id).then(function(reserva) {
        if(reserva) {
            res.status(200).send({ numberOfDays: reserva.getReservationDays() });
        } else {
            throw Error('Reservation not found');
        }
    }).catch(function(error) {
        // log the error
        console.log(error);

        res.status(500).json({
            error: 'Error retrieving days of reservation'
        });
    });
};

module.exports.delete = function(req, res) {
    // Delete reserva
    Reserva.deleteById(req.params.id).then(function() {
        res.status(204).send();
    }).catch(function(error) {
        // log the error
        console.log(error);

        res.status(500).json({
            error: 'Error deleting reserva'
        });
    });
};
