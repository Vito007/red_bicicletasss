const mongoose = require('mongoose');
var connection = mongoose.connection;
const Usuario = require('../../models/usuario');
const Reserva = require('../../models/reserva');
const Bicicleta = require('../../models/bicicleta');

function mongoDBConnection() {
    beforeAll(function(done) {
        if(connection.readyState === 0) {
            connectMongoDB();
        } else {
            // close main connection from app server
            connection.close().then(function() {
                mongoose.disconnect();
                connectMongoDB();
            });
        }
        
        function connectMongoDB() {
            // connect to mongoDB
            mongoose.connect('mongodb://localhost/testdb', {
                useNewUrlParser: true,
                useUnifiedTopology: true
            });
        
            connection = mongoose.connection;
            connection.on('error', console.error.bind(console, 'MongoDB connection error'));
            connection.on('open', function() {
                console.log('Connected to test database');
                done();
            });
        }
    });

    afterEach(function(done) {
        Reserva.deleteMany({}).then(function() {
            Usuario.deleteMany({}).then(function() {
                Bicicleta.deleteMany({}).then(function() {
                    done();
                });
            });
        });
    });

    afterAll(function(done) {
        connection.close().then(function() {
            mongoose.disconnect();
            done();
        });
    });
}

module.exports = mongoDBConnection;
