const Bicicleta = require('../../../models/bicicleta');
const request = require('request');
const server = require('../../../bin/www');
const helperDBConnection = require('../../helpers/mongoDBConnection');

describe('Bicicleta API', function() {
    // this creates and closes the connection in a beforeAll and afterAll methods
    helperDBConnection();

    describe('GET - list bicicletas /', function() {
        it('should return 200', function(done) {
            request.get('http://localhost:3000/api/bicicletas', function(error, response, body) {
                var parsedBody = JSON.parse(body);
            
                expect(response.statusCode).toBe(200);
                expect(parsedBody.bicicletas.length).toBe(0);
                
                done();
            });
        });
    });

    describe('POST - add bicicleta /create', function() {
        it('should return 201', function(done) {
            var bici1 = {
                color: 'naranja',
                modelo: 'NP500',
                lat: -31.4182043,
                lon: -64.1839983
            };

            request.post({
                headers: {'content-type': 'application/json'},
                url: 'http://localhost:3000/api/bicicletas/create',
                body: JSON.stringify(bici1)
            }, function(error, response, body) {
                var parsedBody = JSON.parse(body);
                var expectedBici = {
                    code: 1,
                    color: 'naranja',
                    modelo: 'NP500',
                    ubicacion: [-31.4182043,-64.1839983]
                };
                
                expect(response.statusCode).toBe(201);
                expect(parsedBody.code).toBe(expectedBici.code);
                expect(parsedBody.color).toBe(expectedBici.color);
                expect(parsedBody.modelo).toBe(expectedBici.modelo);
                expect(parsedBody.ubicacion).toEqual(expectedBici.ubicacion);

                Bicicleta.getAllbicicletas().then(function(bicis) {
                    expect(bicis.length).toBe(1);

                    done();
                });
            });
        });
    });

    describe('PUT - update bicicleta /:id/update', function() {
        it('should return 200', function(done) {
            var bici = Bicicleta.createInstance(1, 'rojo', 'raptor', [-31.4158007,-64.1825873]);
            var biciUpdated = {
                color: 'naranja',
                modelo: 'NP500',
                lat: -31.4182043,
                lon: -64.1839983
            };


            // Add bicis to the list
            Bicicleta.add(bici).then(function(newBici) {
                expect(newBici).toEqual(bici);

                request.put({
                    headers: {'content-type': 'application/json'},
                    url: 'http://localhost:3000/api/bicicletas/1/update',
                    body: JSON.stringify(biciUpdated)
                }, function(error, response, body) {
                    var parsedBody = JSON.parse(body);
                    var expectedBici = {
                        code: 1,
                        color: 'naranja',
                        modelo: 'NP500',
                        ubicacion: [-31.4182043,-64.1839983]
                    };
                    
                    expect(response.statusCode).toBe(200);
                    expect(parsedBody.code).toBe(expectedBici.code);
                    expect(parsedBody.color).toBe(expectedBici.color);
                    expect(parsedBody.modelo).toBe(expectedBici.modelo);
                    expect(parsedBody.ubicacion).toEqual(expectedBici.ubicacion);
                    
                    done();
                });
            });
        });
    });

    describe('DELETE - delete bicicleta /:id/delete', function() {
        it('should return 204', function(done) {
            var bici = Bicicleta.createInstance(1, 'rojo', 'raptor', [-31.4158007,-64.1825873]);

            // Add bicis to the list
            Bicicleta.add(bici).then(function(newBici) {
                expect(newBici).toEqual(bici);

                request.delete('http://localhost:3000/api/bicicletas/1/delete', function(error, response, body) {
                    expect(response.statusCode).toBe(204);
                    
                    Bicicleta.getAllbicicletas().then(function(bicis) {
                        // verify the list is empty again
                        expect(bicis.length).toBe(0);
                    
                        done();
                    });
                });
            });
        });
    });
});
