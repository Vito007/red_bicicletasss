const request = require('request');
const server = require('../../../bin/www');
const Reserva = require('../../../models/reserva');
const Bicicleta = require('../../../models/bicicleta');
const Usuario = require('../../../models/usuario');

const helperDBConnection = require('../../helpers/mongoDBConnection');

describe('Reserva API', function() {
    // this creates and closes the connection in a beforeAll and afterAll methods
    helperDBConnection();

    describe('GET - list reservas /', function() {
        it('should return 200', function(done) {
            request.get('http://localhost:3000/api/reservas', function(error, response, body) {
                var parsedBody = JSON.parse(body);
            
                expect(response.statusCode).toBe(200);
                expect(parsedBody.reservas.length).toBe(0);
                
                done();
            });
        });
    });

    describe('DELETE - retrieve days /:id/days', function() {
        it('should return 204', function(done) {
            var usuario = Usuario.createInstance('Marcos');
            var bici = Bicicleta.createInstance(1, 'rojo', 'raptor', [-31.4158007,-64.1825873]);
            var reserva = {
                    "desde": "2020-11-01",
                    "hasta": "2020-11-03",
                    "biciId": bici._id
                };
            
            // save in mongoDB user and bici
            usuario.save();
            bici.save();

            usuario.reserve(reserva.biciId, reserva.desde, reserva.hasta).then(function(newReserva) {
                request.get(`http://localhost:3000/api/reservas/${newReserva._id}/days`, function(error, response, body) {
                    var parsedBody = JSON.parse(body);
            
                    expect(response.statusCode).toBe(200);
                    expect(parsedBody.numberOfDays).toBe(3);

                    done();
                });
            });
        });
    });

    describe('DELETE - delete reserva /:id/reservas', function() {
        it('should return 204', function(done) {
            var usuario = Usuario.createInstance('Marcos');
            var bici = Bicicleta.createInstance(1, 'rojo', 'raptor', [-31.4158007,-64.1825873]);
            var reserva = {
                    "desde": "2020-11-01",
                    "hasta": "2020-11-03",
                    "biciId": bici._id
                };
            
            // save in mongoDB user and bici
            usuario.save();
            bici.save();

            usuario.reserve(reserva.biciId, reserva.desde, reserva.hasta).then(function(newReserva) {
                request.delete(`http://localhost:3000/api/reservas/${newReserva._id}/delete`, function(error, response, body) {
                    expect(response.statusCode).toBe(204);
                    
                    Reserva.getAllReservas().then(function(reservas) {
                        // verify the list is empty again
                        expect(reservas.length).toBe(0);
                    
                        done();
                    });
                });
            });
        });
    });
});
