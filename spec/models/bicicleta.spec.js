const mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');
const helperDBConnection = require('../helpers/mongoDBConnection');
var connection = mongoose.connection;

describe('Bicicleta model', function() {
    // this creates and closes the connection in a beforeAll and afterAll methods
    helperDBConnection();

    describe('Bicicleta.createInstance', function() {
        it('should create a bici with these properties', function() {
            var bici = Bicicleta.createInstance(1, 'rojo', 'raptor', [-31.4158007,-64.1825873]);

            expect(bici).toBeInstanceOf(Object);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe('rojo');
            expect(bici.modelo).toBe('raptor');
            expect(bici.ubicacion).toEqual([-31.4158007,-64.1825873]);
        });
    });

    describe('Bicicleta.getAllbicicletas', function() {
        it('should return an empty list of bicis', function(done) {
            Bicicleta.getAllbicicletas().then(function(bicis) {
                expect(bicis.length).toBe(0);

                done();
            });
        });
    });

    describe('Bicicleta.add', function() {
        it('should add a bici to the list', function(done) {
            var bici = Bicicleta.createInstance(1, 'rojo', 'raptor', [-31.4158007,-64.1825873]);

            Bicicleta.add(bici).then(function(newBici) {
                expect(newBici).toEqual(bici);

                Bicicleta.getAllbicicletas().then(function(bicis) {
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(newBici.code);

                    done();
                });
            });
        });

        it('should add 2 bicis to the list', function(done) {
            var bici1 = Bicicleta.createInstance(1, 'rojo', 'raptor', [-31.4158007,-64.1825873]);
            var bici2 = Bicicleta.createInstance(2, 'naranja', 'captur', [-31.4158047,-64.1825843]);

            Bicicleta.add([bici1, bici2]).then(function(newBicis) {
                expect(newBicis).toEqual([bici1, bici2]);

                Bicicleta.getAllbicicletas().then(function(bicis) {
                    expect(bicis[0].code).toBe(newBicis[0].code);
                    expect(bicis[1].code).toBe(newBicis[1].code);

                    done();
                });
            });
        });
    });

    describe('Bicicleta.update', function() {
        it('should update bici with new values', function(done) {
            var bici = Bicicleta.createInstance(1, 'rojo', 'raptor', [-31.4158007,-64.1825873]);

            Bicicleta.add(bici).then(function(newBici) {
                var updateTo = {
                    code: 1,
                    color: 'naranja',
                    modelo: 'captur',
                    ubicacion: [-31.4158047,-64.1825843]
                };
                
                // validates added bici 
                expect(newBici).toEqual(bici);

                // Update bici
                Bicicleta.update(updateTo).then(function(response) {
                    expect(response.nModified).toBe(1);

                    Bicicleta.findById(bici.code).then(function(biciUpdated) {
                        expect(biciUpdated.color).toBe(updateTo.color);
                        expect(biciUpdated.modelo).toBe(updateTo.modelo);
                        expect(biciUpdated.ubicacion).toEqual(updateTo.ubicacion);

                        done();
    
                    });
                });
            });
        });
    });

    describe('Bicicleta.findById', function() {
        it('should find a bici by id', function(done) {
            var bici1 = Bicicleta.createInstance(1, 'rojo', 'raptor', [-31.4158007,-64.1825873]);
            var bici2 = Bicicleta.createInstance(2, 'naranja', 'captur', [-31.4158047,-64.1825843]);

            Bicicleta.add([bici1, bici2]).then(function(newBicis) {
                expect(newBicis).toEqual([bici1, bici2]);

                Bicicleta.findById(bici1.code).then(function(bici) {
                    expect(bici.code).toBe(bici1.code);

                    done();
                });
            });
        });
    });

    describe('Bicicleta.deleteById', function() {
        it('should delete a bici by id', function(done) {
            var bici1 = Bicicleta.createInstance(1, 'rojo', 'raptor', [-31.4158007,-64.1825873]);
            var bici2 = Bicicleta.createInstance(2, 'naranja', 'captur', [-31.4158047,-64.1825843]);

            Bicicleta.add([bici1, bici2]).then(function(newBicis) {
                expect(newBicis).toEqual([bici1, bici2]);

                Bicicleta.deleteById(bici1.code).then(function(response) {
                    expect(response.deletedCount).toBe(1);

                    Bicicleta.getAllbicicletas().then(function(bicis) {
                        expect(bicis.length).toBe(1);
                        expect(bicis[0].code).toBe(bici2.code);
    
                        done();
                    });
                });
            });
        });
    });
});
