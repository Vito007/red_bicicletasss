const Bicicleta = require('../../models/bicicleta');
const Reserva = require('../../models/reserva');
const Usuario = require('../../models/usuario');
const helperDBConnection = require('../helpers/mongoDBConnection');

describe('Usuario model', function() {
    // this creates and closes the connection in a beforeAll and afterAll methods
    helperDBConnection();

    describe('Usuario.createInstance', function() {
        it('should create an usuario with these properties', function() {
            var usuario = Usuario.createInstance('Marcos');

            expect(usuario).toBeInstanceOf(Object);
            expect(usuario.nombre).toBe('Marcos');
        });
    });

    describe('Usuario.reserve', function() {
        it('should create a reservation with these properties', function(done) {
            var usuario = Usuario.createInstance('Marcos');
            var bici = Bicicleta.createInstance(1, 'rojo', 'raptor', [-31.4158007,-64.1825873]);
            var reserva = {
                    "desde": "2020-11-01",
                    "hasta": "2020-11-03",
                    "biciId": bici._id
                };
            
            // save in mongoDB user and bici
            usuario.save();
            bici.save();

            usuario.reserve(reserva.biciId, reserva.desde, reserva.hasta).then(function(newReserva) {
                expect(newReserva).toBeInstanceOf(Object);
                expect(newReserva.bicicleta).toEqual(bici._id);

                // Find all reservations
                Reserva.find({}).populate('usuario').populate('bicicleta').exec().then(function(reservas) {
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe('Marcos');
                    expect(reservas[0].bicicleta.color).toBe('rojo');

                    done();
                });
            });
        });
    });
});
