var express = require('express');
var router = express.Router();
var reservaApiController = require('../../controllers/api/reservaApi');

// list
router.get('/', reservaApiController.list);
// numberOfDays
router.get('/:id/days', reservaApiController.days);
// delete
router.delete('/:id/delete', reservaApiController.delete);

module.exports = router;
