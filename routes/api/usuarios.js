var express = require('express');
var router = express.Router();
var usuarioApiController = require('../../controllers/api/usuarioApi');

// list
router.get('/', usuarioApiController.list);
// add
router.post('/create', usuarioApiController.create);
// reserve
router.post('/:id/reserve', usuarioApiController.reserve);
// update
router.put('/:id/update', usuarioApiController.update);
// delete
router.delete('/:id/delete', usuarioApiController.delete);

module.exports = router;
